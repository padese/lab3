import json
import os
from report import Report
from custom_exceptions import NoConfigFileError
class Config(object):

    """
    Class for smartrm configs
    """

    def __init__(self):
        """
        restore_policy: 'replace', 'restore', 'ask'
        clean_policy: 'always', 'optional', 'never'
        tr_dir: directory to use as trash folder
        max_tr_size: max size of trash
        rem_period: for clean policy,
                    period in days to store file in trash,
                    0 to skip
        rem_size: for clean policy,
                  max size of file to store in trash,
                  0 to skip
        confirm: ask a confirm before rm a file,
                 matches -i argument
        dryrun: safe dryrun mode
        notrash: remove files with no trash
        report: output info messages
        force: don't ask user's prompt
        """
        self.restore_policy = 'replace'
        self.clean_policy = 'always'

        self.tr_dir = os.path.expanduser('~/trash')
        self.max_tr_size = 100

        self.rem_period = 10
        self.rem_size = 10

        self.confirm = True
        self.dryrun = False
        self.notrash = False
        self.report = True
        self.force = False

        self.config_dir = os.path.expanduser('~/smrm.config')

    def set_from_args(self, args):
        """
        Change config values depending on command line args
        :param args: command line args
        """
        for attr in vars(args):
            if hasattr(self, attr):
                if getattr(args, attr) is not None and getattr(args, attr) != getattr(self, attr):
                    self.__setattr__(attr, getattr(args, attr))

    def tojson(self):
        """
        Save current config values to config file
        """
        with open(self.config_dir, 'w') as f:
            json.dump(self.__dict__, f)

    def fromjson(self):
        """
        Load values from config files
        """
        if not os.path.exists(self.config_dir):
            raise NoConfigFileError()

        with open(self.config_dir) as json_data_file:
            info = json.load(json_data_file)
        for attr in info.keys():
            if hasattr(self, attr):
                self.__setattr__(attr, info[attr])

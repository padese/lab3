import argparse


class ParseCmdArgs(object):
    @staticmethod
    def str_to_bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        if v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    @staticmethod
    def parse(cmd_args):
        """
        Parses command line arguments and saves to args
        Command line usage:
            smrm {rem, trash, config}
        :param cmd_args: command line arguments
        :return: args
        """
        parser = argparse.ArgumentParser(description='Smart rm')
        subparser = parser.add_subparsers(dest='command')

        # subparser for remove functions
        rem = subparser.add_parser('rem',
                        help='Remove functions. By defult all files are removed to trash if --notrash is not set')

        rem.add_argument('files', metavar='files', type=str, nargs='*',
                         help='files to remove')
        rem.add_argument('-r', action='store_true',
                         help='recursively delete files and empty folders in given directory')
        rem.add_argument('-I', action='store_true',
                         help='ask a confirm if more than 3 files are deleted or -r is set')
        rem.add_argument('-d', action='store_true',
                         help='remove empty directory')
        rem.add_argument('-t', action='store_true',
                         help='remove directory tree')

        rem.add_argument('-i', dest='confirm', type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                         help='ask a confirm every time')
        rem.add_argument('-f', '--force', type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                         help='force remove: no confirms asked, nothing is reported')
        rem.add_argument('--regex',   action='store',
                         help='recursive remove files mathing to regex in given directory')
        rem.add_argument('--notrash', type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                         help='delete without moving to trash')
        rem.add_argument('--dryrun',  type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                         help='safe dry-run mode, doesnt make any changes')
        rem.add_argument('--report',  type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                         help='report of done actions')

        # subparser for trash functions
        trash_subp = subparser.add_parser('trash', help='Trash functions')
        trash_subp.add_argument('files', metavar='files', type=str, nargs='*', help='files')

        trash_subp.add_argument('-v', action='store_true',
                                help='view trash')
        trash_subp.add_argument('-c', action='store_true',
                                help='clean trash')
        trash_subp.add_argument('-r', action='store_true',
                                help='restore given file')
        trash_subp.add_argument('-d', action='store_true',
                                help='delete file from trash permanently')

        trash_subp.add_argument('-f', '--force', type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                                help='force: no confirms asked, nothing is reported')
        trash_subp.add_argument('--dryrun', type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                                help='safe dry-run mode, doesnt make any changes')
        trash_subp.add_argument('--report', type=ParseCmdArgs.str_to_bool, const='True', nargs='?',
                                help='report of done actions')

        # subparser for config values
        conf_subp = subparser.add_parser('config', help='config settings')

        conf_subp.add_argument('--restore_policy', action='store', choices=['ask', 'replace', 'saveboth'],
                               help='restore policy')
        conf_subp.add_argument('--clean_policy', action='store', choices=['always', 'optional', 'never'],
                               help='clean policy')

        conf_subp.add_argument('--tr_dir',      type=str, action='store',
                               help='trash directory')
        conf_subp.add_argument('--max_tr_size', type=int, action='store',
                               help='trash max size')
        conf_subp.add_argument('--rem_size',    type=int, action='store',
                               help='max file size to store in trash')
        conf_subp.add_argument('--rem_period',  type=int, action='store',
                               help='period to store file in trash')

        conf_subp.add_argument('-i', '--interactive', dest='confirm', type=ParseCmdArgs.str_to_bool,
                                action='store', help='confirm every time')
        conf_subp.add_argument('-f','--force', action='store', type=ParseCmdArgs.str_to_bool,
                               help='force mode')
        conf_subp.add_argument('--notrash', action='store', type=ParseCmdArgs.str_to_bool,
                               help='notrash mode')
        conf_subp.add_argument('--dryrun',  action='store', type=ParseCmdArgs.str_to_bool,
                               help='safe dry-run mode')
        conf_subp.add_argument('--report',  action='store', type=ParseCmdArgs.str_to_bool,
                               help='report')

        args = parser.parse_args(cmd_args)

        if args.command == 'rem':
            if args.force:
                args.confirm = False
                args.I = False
                args.report = False
            if args.confirm:
                args.I = False

        return args

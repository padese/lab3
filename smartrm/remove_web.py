import multiprocessing
from trash import Trash
import os
import re
from custom_exceptions import *
from config import Config
from clean_policy import CleanPolicy

class RemoveWeb(object):

    def __init__(self, tr_dir, max_tr_size):
        self.tr_dir = tr_dir
        self.trash = Trash(self.tr_dir)
        self.max_tr_size = max_tr_size

    def remove(self, files, num_proc, regex=None):

        self.trash.check_create()
        c = Config()
        c.rem_period = 10
        c.rem_size = 10
        clean_policy = CleanPolicy(c, self.trash)
        clean_policy.run()

        if self.check_args_size(files) == 1:
            return [], 'err_space'

        result = Producer.run(self.trash, files, num_proc, regex)
        errors = ''
        processed_files = []
        for k in result.keys():
            processed_files.append(k)
            if result[k]:
                errors += '1,'
            else:
                errors += '0,'
        return processed_files, errors

    def check_args_size(self, files):
        files_size = sum([self.trash.dir_size(file_) for file_ in files])
        if files_size > self.max_tr_size - self.trash.get_size():
            return 1


class Producer(object):
    @staticmethod
    def run(trash, files, num_proc, regex=None):
        files_queue = multiprocessing.JoinableQueue()
        results = multiprocessing.Queue()
        lock = multiprocessing.Lock()
        consumers = [Consumer(files_queue, results, trash, lock)
                     for i in xrange(num_proc)]
        for c in consumers:
            c.start()
        num_files = len(files)

        if regex:
            for f in files:
                Producer.regex_find(f, regex, files_queue)
        else:
            for i in xrange(num_files):
                files_queue.put(files[i])

        for i in xrange(num_proc):
            files_queue.put(None)

        files_queue.join()

        result = {}
        while not results.empty():
            x = results.get()
            result[x[0]] = x[1]
        return result

    @staticmethod
    def regex_find(path, regex, files_queue):
        if os.path.isdir(path) and os.listdir(path).__len__() != 0:
            for file_ in os.listdir(path):
                Producer.regex_find(os.path.join(path, file_), regex, files_queue)
        else:
            r = re.search(regex, os.path.basename(path))
            if r is not None:
                print 'put '+os.path.basename(path)
                files_queue.put(path)

class Consumer(multiprocessing.Process):
    def __init__(self, task_queue, result_queue, trash, lock):
        multiprocessing.Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue
        self.trash = trash
        self.lock = lock

    def run(self):
        proc_name = self.name
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                self.task_queue.task_done()
                break
            print '%s: %s' % (proc_name, next_task)

            try:
                self.trash.remove(next_task, self.lock)
            except NoFileError:
                self.result_queue.put((next_task, 1))
            else:
                self.result_queue.put((next_task, 0))
            self.task_queue.task_done()
        return



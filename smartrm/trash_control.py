import sys
from trash_action import TrashAction
from trash import Trash
from report import Report
from confirm import Confirm
from custom_exceptions import *


class TrashControl(object):

    """
    Control class for trash
        args: command line arguments
        config: smartrm configs
    """

    def __init__(self, args, config):
        self.args = args
        self.report = Report(config.report, config.force)
        self.confirm = Confirm(config.confirm)
        self.trash = Trash(config.tr_dir)
        self.trash_action = TrashAction(self.trash, config, self.report)

    def run(self):
        """
        Call Trash methods depending on cmd args
        """
        exit_error = False

        if not self.trash.exists():
            self.report.error('trash folder not found')
            exit_error = True

        if self.args.v:
            self.trash_action.view_trash()
        if self.args.c:
            self.trash_action.clean_trash()
        if self.args.r:
            for file_ in self.args.files:
                try:
                    self.trash_action.restore(file_)
                except (NoFileInTrashError, CannotRestoreError) as e:
                    self.report.error(e)
                    exit_error = True

        if self.args.d:
            for file_ in self.args.files:
                try:
                    self.trash_action.del_from_trash(file_)
                except NoFileInTrashError as e:
                    self.report.error(e)
                    exit_error = True

        if exit_error:
            sys.exit(1)
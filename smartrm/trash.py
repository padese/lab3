import os
import shutil
from datetime import datetime, timedelta
from restore_policy import RestorePolicy
import multiprocessing
from custom_exceptions import *


class Trash(object):

    def __init__(self, tr_dir):
        self.tr_dir = tr_dir

    def remove(self, path, lock=None):

        if not os.path.exists(path):
            raise NoFileError(path)

        if lock is None:
            path_name = self.duplicate_check(os.path.join(self.tr_dir, 'files'), os.path.basename(path))
            os.rename(path, os.path.join(self.tr_dir, 'files', path_name))
        else:
            with lock:
                path_name = self.duplicate_check(os.path.join(self.tr_dir, 'files'), os.path.basename(path))
                os.rename(path, os.path.join(self.tr_dir, 'files', path_name))

        with open(os.path.join(self.tr_dir, 'info', path_name + '.trashinfo'), 'w+') as info_file:
            info_file.write("[Trash Info]\n")
            info_file.write(os.path.abspath(path) + '\n')
            info_file.write(datetime.now().__str__())
        #s = 'removed {} by {}'.format(path, multiprocessing.current_process().name)
        #print s

    @staticmethod
    def duplicate_check(dir_, file_):
        if not os.path.exists(os.path.join(dir_, file_)):
            return file_
        ind = 1
        while os.path.exists(os.path.join(dir_, file_ + '.{}'.format(ind))):
            ind += 1
        file_ += '.' + ind.__str__()
        return file_

    def get_files(self):
        """
        :return: list of files located in trash
        """
        files = []
        for f in os.listdir(os.path.join(self.tr_dir, 'files')):
            files.append(f)
        return files

    def get_size(self):
        """
        :return: trash size, MBs
        """
        return self.dir_size(self.tr_dir)

    def dir_size(self, path):
        """
        :return: size of path file or directory, MBs
        """
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        return total_size / 1024 / 1024

    def restore(self, file_, restore_policy=RestorePolicy('saveboth')):
        """
        Restores a file from trash to previous directory
        If file with the same name already exists,
        restore policy runs
        """
        with open(os.path.join(self.tr_dir, 'info', file_ + '.trashinfo'), 'r') as info_file:
            info_file.readline()
            dir_ = info_file.readline().rstrip()
        dirs = dir_.split('/')
        p = '/'
        for d in dirs[1:-1]:
            p = os.path.join(p, d)
            if not os.path.exists(p):
                os.mkdir(p)

        if os.path.exists(dir_):
            dir_ = restore_policy.run(dir_)

        os.rename(os.path.join(self.tr_dir, 'files', file_), dir_)
        os.remove(os.path.join(self.tr_dir, 'info', file_ + '.trashinfo'))

    def clean(self):
        """
        Deletes all files from trash
        """
        prev_dir = os.path.abspath(os.curdir)
        os.chdir(self.tr_dir)
        shutil.rmtree('files')
        os.mkdir('files')
        shutil.rmtree('info')
        os.mkdir('info')
        os.chdir(prev_dir)

    def delete_from_trash(self, file_):
        """
        Delete file from trash permanently
        """
        if os.path.isdir(os.path.join(self.tr_dir, 'files', file_)):
            shutil.rmtree(os.path.join(self.tr_dir, 'files', file_))
        else:
            os.remove(os.path.join(self.tr_dir, 'files', file_))
        os.remove(os.path.join(self.tr_dir, 'info', file_ + '.trashinfo'))

    def check_create(self):
        """
        Creates trash folder if it doesn't exist
        """
        if not os.path.exists(self.tr_dir):
            os.makedirs(self.tr_dir)
        if not os.path.exists(os.path.join(self.tr_dir, 'files')):
            os.makedirs(os.path.join(self.tr_dir, 'files'))
        if not os.path.exists(os.path.join(self.tr_dir, 'info')):
            os.makedirs(os.path.join(self.tr_dir, 'info'))

    def exists(self):
        """
        Check if trash folder exists and has right structure
        :return: True if trash folder exists, else False
        """
        if not os.path.exists(self.tr_dir):
            return False
        if not os.path.exists(os.path.join(self.tr_dir, 'files')):
            return False
        if not os.path.exists(os.path.join(self.tr_dir, 'info')):
            return False
        return True

    def files_count(self):
        """
        :return: number of files in trash
        """
        return len(os.listdir(os.path.join(self.tr_dir, 'files')))

    def rm_bigger_than(self, max_size):
        """
        Find all files bigger than max_size
        and remove then
        :param max_size:
        """
        size = {}
        for file_ in os.listdir(os.path.join(self.tr_dir, 'files')):
            s = self.dir_size(os.path.join(self.tr_dir, 'files', file_))
            size[s] = file_
        for f in size.keys():
            if f > max_size:
                self.delete_from_trash(size[f])

    def rm_older_than(self, days):
        """
        Find all files removed more than days ago
        and remove then
        :param days:
        """
        dates = {}
        period = timedelta(int(days))
        for file_ in os.listdir(os.path.join(self.tr_dir, 'info')):
            with open(os.path.join(self.tr_dir, 'info', file_), 'r') as info_file:
                info_file.readline()
                info_file.readline()
                da = datetime.strptime(info_file.readline().rstrip(), '%Y-%m-%d %H:%M:%S.%f')
            dnow = datetime.now()
            if dnow - da > period:
                dates[da] = file_[:-10]
            info_file.close()
        for f in dates.keys():
            self.delete_from_trash(dates[f])
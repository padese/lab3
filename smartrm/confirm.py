def force(func):
    def inner(*args):
        self = args[0]
        if self.confirm:
            return func(*args)
        else:
            return True

    return inner


class Confirm(object):

    """
    Class for asking a congirm
        confirm: when True, asks a confirm
                when False, returns positive answer
        force: when True doesn't ask anything,
                returns positive answer
    Uses force decorator
    """

    def __init__(self, _confirm,):
        self.confirm = _confirm

    @staticmethod
    def str_to_bool(ans):
        if ans.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        else:
            return False

    @force
    def prompt(self, mes):
        """
        Asks a prompt for remove/restore action
        :param mes: info message
        :return: True to allow the action, False else
        """
        print mes
        if self.str_to_bool(raw_input()) is True:
            return True
        return False

    @force
    def prompt_before_rm(self, f_num, rec):
        """
        Matches '-I' argument
        Asks a prompt if more than 3 files are given
        or '-r' is set
        """
        if len(f_num) > 3 or rec:
            print "remove {0} arguments?".format(len(f_num))
            if self.str_to_bool(raw_input()) is True:
                return True
            else:
                return False
        return True

    @staticmethod
    def prompt_config():
        """
        Run when config file is not found. Asks a prompt
        to create new config file with default settings
        :param mes: info message
        :return: True to allow the action, False else
        """
        print 'Config file not found. Create new with default settings?'
        if Confirm.str_to_bool(raw_input()) is True:
            return True
        return False



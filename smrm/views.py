# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from models import Trash, Task
from django.http import HttpResponseRedirect, HttpResponse
from django.views import generic, View
from trashform import TrashForm
from taskform import TaskForm
from smartrm.trash import Trash as SmTrash


class IndexViewTrash(generic.ListView):
    template_name = 'smrm/index_trash.html'
    context_object_name = 'trash_list'

    def get_queryset(self):
        return Trash.objects.all()


class IndexViewTask(generic.ListView):
    template_name = 'smrm/index_task.html'
    context_object_name = 'task_list'

    def get_queryset(self):
        return Task.objects.all()


class DetailViewTrash(generic.DetailView):
    model = Trash
    template_name = 'smrm/detail_trash.html'

    def post(self, request, pk):
        if 'del' in request.POST:
            file = request.POST.get('del')
            path = Trash.objects.get(id=pk).path
            SmTrash(path).delete_from_trash(file)
            return HttpResponseRedirect('')

        if 'rest' in request.POST:
            file = request.POST.get('rest')
            path = Trash.objects.get(id=pk).path
            SmTrash(path).restore(file)
            return HttpResponseRedirect('')

        if 'clean' in request.POST:
            path = Trash.objects.get(id=pk).path
            SmTrash(path).clean()
            return HttpResponseRedirect('')

        if 'deltrash' in request.POST:
            trash = Trash.objects.get(id=pk)
            path = trash.path
            SmTrash(path).clean()
            trash.delete()
            return HttpResponseRedirect('/trash')


class DetailViewTask(generic.DetailView):
    model = Task
    template_name = 'smrm/detail_task.html'

    def post(self, request, pk):
        if 'run' in request.POST:
            task = Task.objects.get(id=pk)
            task.run()
            return HttpResponseRedirect('')

        if 'delete' in request.POST:
            task = Task.objects.get(id=pk)
            task.delete()
            return HttpResponseRedirect('/task')


def addtrash(request):
    if request.method == 'POST':
        form = TrashForm(request.POST)
        if form.is_valid():
            t = Trash(name=form.cleaned_data['name'],
                      path=form.cleaned_data['path'],
                      restore_policy=form.cleaned_data['restore_policy'],
                      max_size=form.cleaned_data['max_size'],
                      autoclean=form.cleaned_data['autoclean'],
                      rem_period=form.cleaned_data['rem_period'],
                      rem_size=form.cleaned_data['rem_size'])
            path = form.cleaned_data['path']
            SmTrash(path).check_create()
            t.save()
            return HttpResponseRedirect('/')
    else:
        form = TrashForm()

    return render(request, 'smrm/trashform.html', {'form': form})


def addtask(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            t = Task(trash=Trash.objects.get(id=form.cleaned_data['trash']),
                     files=form.cleaned_data['files'],
                     description=form.cleaned_data['description'],
                     proc=form.cleaned_data['proc'],
                     regex=form.cleaned_data['regex'])
            t.save()
            return HttpResponseRedirect('/task')
    else:
        form = TaskForm()

    return render(request, 'smrm/taskform.html', {'form': form})



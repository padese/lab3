from jinja2 import Template
from models import Trash
import os


def treebuild(path):
    t = Template("""<ul>
{% for item in tree.dirs recursive %}
    <li id='{{item.fname}}' data-jstree='{"type":"default"}'> {{ item.name }}
    {%- if item.dirs -%}
        <ul>{{ loop(item.dirs) }}</ul>
    {%- endif %}  

    {% if item.files %}
        <ul>
        {% for item in item.files %}
            <li id='{{item.fname}}' data-jstree='{"type":"file"}'> {{ item.name }}</li>
        {% endfor %}
        </ul>
    {% endif %}</li>
{%- endfor %}
{% for item in tree.files %}
    <li id='{{item.fname}}' data-jstree='{"type":"file"}'> {{ item.name }}</li>
{% endfor %}
</ul>""")
    tree = make_tree(path, 0)
    return t.render(tree=tree)



def make_tree(path, depth):
    max_depth = 4
    max_files = 15
    tree = dict(name=os.path.basename(path), fname=os.path.abspath(path), dirs=[], files=[])
    if depth > max_depth:
        return tree
    try:
        lst = os.listdir(path)
    except OSError:
        pass
    else:
        for name in lst:
            if name.startswith('.'):
               continue
            if len(tree['dirs']) + len(tree['files']) > max_files and depth > 0:
                return tree
            fname = os.path.join(path, name)
            if fname in [x.path for x in Trash.objects.all()]:
                continue
            if os.path.isdir(fname):
                tree['dirs'].append(make_tree(fname, depth+1))
            else:
                tree['files'].append(dict(name=name, fname=fname, ))
    return tree
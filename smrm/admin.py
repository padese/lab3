# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Trash


class TrashAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['path','name']}),
    ]

admin.site.register(Trash, TrashAdmin)
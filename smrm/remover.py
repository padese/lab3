import multiprocessing
import time
from smartrm.trash import Trash
from smartrm.custom_exceptions import NoFileError

class Consumer(multiprocessing.Process):
    def __init__(self, task_queue, result_queue, trash, lock):
        multiprocessing.Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue
        self.trash = trash
        self.lock = lock

    def run(self):
        proc_name = self.name
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                self.task_queue.task_done()
                break
            print '%s: %s' % (proc_name, next_task)

            try:
                self.trash.remove(next_task, self.lock)
            except NoFileError:
                self.result_queue.put(next_task)
            self.task_queue.task_done()
        return

class Remover(object):

    def run(self, dir_path, files, num_consumers):
        tasks = multiprocessing.JoinableQueue()
        results = multiprocessing.Queue()
        lock = multiprocessing.Lock()

        trash = Trash(dir_path)
        consumers = [Consumer(tasks, results, trash, lock)
                     for i in xrange(num_consumers)]
        for c in consumers:
            c.start()

        jobs = files
        num_jobs = len(jobs)
        for i in xrange(num_jobs):
            tasks.put(jobs[i])
        for i in xrange(num_consumers):
            tasks.put(None)

        tasks.join()

        result = []
        while not results.empty():
            result.append(results.get())

        errors = ''
        for f in files:
            if f in result:
                errors += '1,'
            else:
                errors += '0,'

        print results
        return errors
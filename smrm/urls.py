from django.conf.urls import url
import views

app_name = 'smrm'

urlpatterns = [

    url(r'^$', views.IndexViewTrash.as_view(), name='trash'),
    url(r'^trash/$', views.IndexViewTrash.as_view(), name='trash'),
    url(r'^trash/(?P<pk>[0-9]+)/$', views.DetailViewTrash.as_view(), name='detailtrash'),
    url(r'^task/(?P<pk>[0-9]+)/$', views.DetailViewTask.as_view(), name='detailtask'),
    url(r'^trash/new/$', views.addtrash, name='newtrash'),
    url(r'^task/new/$', views.addtask, name='newtask'),
    url(r'^task/$', views.IndexViewTask.as_view(), name='task'),
   
]

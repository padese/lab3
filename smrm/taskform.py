# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django import forms
from treebuild import treebuild
from models import Trash


class TaskForm(forms.Form):
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 2, 'cols': 25}), max_length=200,required=False)
    trash = forms.ChoiceField(
        choices=[(x.id, '{} ({})'.format(x.name,x.path)) for x in Trash.objects.all()])
    files = forms.Field(widget=forms.HiddenInput, required=False)
    proc = forms.IntegerField(min_value=1, initial=8, label='Processes')
    regex = forms.CharField(required=False)

    def maketree(self):
        return treebuild('/home/denis')
$(document).ready(function(){
    $('#mbut').mousedown(function(){
        var selectedElmsIds = [];
        var selectedElms = $('#tree').jstree("get_top_selected", true);
        $.each(selectedElms, function() {
            selectedElmsIds.push(this.id);
        });
        $('#id_files').val(selectedElmsIds);
    });
});

$(function () {
    $("#tree").jstree({
        "checkbox" : {"keep_selected_style" : false},

        "types" : {
            "default" : {
            },
            "file" : {
              "icon" : "glyphicon glyphicon-file"
            }
        },
        "plugins" : [ "checkbox" , "types"]
    });
});
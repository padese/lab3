from django import forms


class TrashForm(forms.Form):
    path = forms.FilePathField(allow_folders=True,  allow_files=False, match='^[^./].+',  path=u'/home/denis/')
    name = forms.CharField(max_length=200)
    restore_policy = forms.ChoiceField(choices=((1, 'ask'), (2, 'replace')))
    max_size = forms.IntegerField(min_value=0, max_value=4000, initial=100)
    autoclean = forms.BooleanField(required=False)
    rem_period = forms.IntegerField(initial=-1, min_value=-1, label='Autoclean remove period')
    rem_size = forms.IntegerField(initial=-1, min_value=-1, label='Autoclean remove size')
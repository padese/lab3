# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from smartrm.trash import Trash as SmTrash
import os
from remover import Remover
from smartrm.remove_web import RemoveWeb

class Trash(models.Model):
    RESTORE_POLICIES = ((1, 'saveboth'),
                        (2, 'replace'))
    name = models.CharField(max_length=200, default='new trash')
    path = models.CharField(max_length=200)
    restore_policy = models.IntegerField(choices=RESTORE_POLICIES, default=1)
    max_size = models.IntegerField(default=100)
    autoclean = models.BooleanField(default=False)
    rem_period = models.IntegerField(default=-1)
    rem_size = models.IntegerField(default=-1)

    def __str__(self):
        return self.path

    def get_size(self):
        return SmTrash(self.path).get_size()

    def get_files_count(self):
        return SmTrash(self.path).files_count()

    def get_files(self):
        tr = SmTrash(self.path)
        files = tr.get_files()
        types = [self.file_type(f) for f in files]
        dict = [{'name':files[i],'type':types[i]} for i in range(len(files))]
        return dict

    def file_type(self, file):
        file = os.path.join(self.path,'files', file)
        if os.path.isdir(file):
            if len(os.listdir(file)) == 0:
                return 'empty folder'
            else:
                return 'folder'
        return 'file'


class Task(models.Model):
    STATUS_CHOICES = (
        (1, 'ready to run'),
        (2, 'processing'),
        (3, 'completed: OK'),
        (4, 'completed: error'),
        (5, 'not enough free space')
    )
    trash = models.ForeignKey(Trash)
    description = models.CharField(max_length=200)
    files = models.CharField(max_length=400)
    status = models.IntegerField(default=1, choices=STATUS_CHOICES)
    proc = models.IntegerField(default=8)
    errors = models.CharField(max_length=400, default='')
    regex = models.CharField(max_length=30, default='')

    def files_as_list(self):
        return self.files.split(',')

    def files_num(self):
        return len(self.files_as_list())

    def get_errors(self):
        return self.errors.split(',')

    def isdisabled(self):
        if self.status == 3 or self.status == 4:
            return 'disabled'

    def get_status_color(self):
        if self.status == 1:
            return 'blue'
        if self.status == 3:
            return 'green'
        if self.status == 4 or self.status == 5:
            return 'red'

    def get_files(self):
        files = self.files_as_list()
        types = []
        if self.status == 1 or self.status == 5:
            for i in range(self.files_num()):
                types.append('')
        else:
            for i in range(self.files_num()):
                if self.get_errors()[i] == '1':
                    types.append('error')
                else:
                    types.append('removed')
        dict = [{'name': files[i], 'result': types[i]} for i in range(len(files))]
        return dict

    def run(self):
        p = RemoveWeb(self.trash.path, self.trash.max_size)
        files, result = p.remove(self.files_as_list(), self.proc, self.regex)
        if result == 'err_space':
            self.status = 5
            self.save(update_fields=['status'])
            return
        if result.find('1') == -1:
            self.status = 3
        else:
            self.status = 4
        self.errors = result
        self.files = ','.join(files)
        self.save(update_fields=['status', 'errors', 'files'])
